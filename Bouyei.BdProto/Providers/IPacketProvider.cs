﻿using System;
using System.Collections.Generic;

using System.Text;


namespace Bouyei.BdProto
{
    using Structures;

    public interface IPacketProvider
    {
        byte[] Encode(PacketFrom item);

        PacketMessage Decode(byte[] buffer, int offset, int count);
    }
}
